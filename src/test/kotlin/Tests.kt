package org.example.sandox

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class Tests {
    @Test
    fun `2 + 2 = 4`() {
        assertEquals(4, 2 + 2, "2 + 2 should equal 4")
    }
    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource("0,    1,   1", "1,    2,   3", "49,  51, 100", "1,  100, 101")
    fun addTest(first: Int, second: Int, expectedResult: Int) {
        val calculator = Calculator()
        assertEquals(expectedResult, calculator.add(first, second)) {
            "$first + $second should equal $expectedResult"
        }
    }
}
