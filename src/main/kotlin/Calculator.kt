package org.example.sandox

class Calculator {
    /**
     * Add additions
     * @param a first addition.
     * @param b second addition.
     * @return a + b
     */
    fun add(a: Int, b: Int) = a + b
}
